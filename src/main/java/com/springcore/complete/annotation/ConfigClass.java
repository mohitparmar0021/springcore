package com.springcore.complete.annotation;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


//@ComponentScan(basePackages = "com/springcore/complete/annotation")
@Configuration
public class ConfigClass {
    
	@Bean(name="emp")
	public Employee getEmpObject() {
		return new Employee();
	}
	
	
	
}
