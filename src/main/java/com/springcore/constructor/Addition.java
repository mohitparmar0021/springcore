package com.springcore.constructor;

public class Addition {
      private int a;
      private int b;

	public Addition(int a, int b) {
		super();
		System.out.println("inside int ");
		this.a = a;
		this.b = b;
	}
    
	public Addition(double a, double b) {
		System.out.println("inside double");
		this.a=(int) a;
		this.b=(int) b;
	}
	
    public void doSum() {
    	
    	System.out.println("Sum of a and b : " + a + " + " + b  + " : " + (a+b));
    }
      
}
