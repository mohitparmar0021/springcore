package com.springcore.constructor;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Check {

	public static void main(String[] args) {
		
	ApplicationContext context = new ClassPathXmlApplicationContext("com/springcore/constructor/constructorConfig.xml");
	  // Person p1=context.getBean("person1", Person.class);
	 //  Person p2=context.getBean("person2", Person.class);
	   
	 //  System.out.println(p1);
	 //  System.out.println(p2);
		
	    Addition a1= context.getBean("add1", Addition.class);
	    a1.doSum();
	}

}
