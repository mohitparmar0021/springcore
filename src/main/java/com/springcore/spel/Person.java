package com.springcore.spel;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Person {
	
	    @Value("Mohit Parmar")
        private String name;
	    
	    @Value("21")
        private int age;
        
	    
	    @Value("#{T(java.lang.Math).sqrt(25)}")
	    private double z;
        
	    
		public double getZ() {
			return z;
		}

		public void setZ(double z) {
			this.z = z;
		}

		@Override
		public String toString() {
			return "Person [name=" + name + ", age=" + age + ", z=" + z + "]";
		}
		
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public int getAge() {
			return age;
		}
		public void setAge(int age) {
			this.age = age;
		}
        
        
}
