package com.springcore.standalone.collection;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestDemo {

	public static void main(String[] args) {
	ApplicationContext context = new ClassPathXmlApplicationContext("com/springcore/standalone/collection/standaloneConfig.xml");
        Person p1 = context.getBean("person1" , Person.class);
        System.out.println(p1);
        
       p1.display();
       System.out.println(p1.getFriends().getClass().getName());
	}

}
