package com.springcore.lifecycle;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {

	public static void main(String[] args) {
		AbstractApplicationContext context = new ClassPathXmlApplicationContext("com/springcore/lifecycle/configLifeCycle.xml");
		
		//Samosa s1 = context.getBean("s1", Samosa.class);
	//	System.out.println(s1);
		
	//	Pepsi p1=context.getBean("p1", Pepsi.class);
	//	System.out.println(p1);
		
		Jalebi j=context.getBean("j1", Jalebi.class);
		System.out.println(j);
		
		context.registerShutdownHook();
	}

}
