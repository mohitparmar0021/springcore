package com.springcore.lifecycle;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

public class Jalebi {
       private double price;

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "Jalebi [price=" + price + "]";
	}
    
    @PostConstruct
    public void startInit() {
    	System.out.println("this is init method");
    }
    
    @PreDestroy
    public void endDestroy() {
		System.out.println("this is destroyed method");
	}
       
}
