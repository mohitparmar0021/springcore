package com.springcore.collections;

import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

public class Employee {

	private String name;

	private List<Integer> mobileNumber;
	private Set<String> addresses;
	private Map<String, String> dept;
	private Properties props;
	

	public Properties getProps() {
		return props;
	}

	public void setProps(Properties props) {
		this.props = props;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Integer> getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(List<Integer> mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public Set<String> getAddresses() {
		return addresses;
	}

	public void setAddresses(Set<String> addresses) {
		this.addresses = addresses;
	}

	public Map<String, String> getDept() {
		return dept;
	}

	public void setDept(Map<String, String> dept) {
		this.dept = dept;
	}

	@Override
	public String toString() {
		return "Employee [name=" + name + ", mobileNumber=" + mobileNumber + ", addresses=" + addresses + ", dept="
				+ dept + ", props=" + props + "]";
	}

}
