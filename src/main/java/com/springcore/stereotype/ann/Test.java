package com.springcore.stereotype.ann;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {

	public static void main(String[] args) {
	   ApplicationContext context = new ClassPathXmlApplicationContext("com/springcore/stereotype/ann/stereoConfig.xml");
	 
	   Student student = context.getBean("std1" , Student.class);
	   
	   Student student1 = context.getBean("std1" , Student.class);
	   
       System.out.println(student1);
	   System.out.println(student.hashCode());
	   System.out.println(student1.hashCode());
	   
	}

}
