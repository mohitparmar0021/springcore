package com.springcore.stereotype.ann;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

//@Component("obj")
//@Scope("prototype")
public class Student {
     
  //  @Value("Mohit Parmar")	
	private String name;
    
    
 //   @Value("#{myFriend}")
    private List<String> friends;

	public List<String> getFriends() {
		return friends;
	}

	public void setFriends(List<String> friends) {
		this.friends = friends;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Student [name=" + name + ", friends=" + friends + "]";
	}
      
      
}
