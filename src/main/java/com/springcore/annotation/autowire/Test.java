package com.springcore.annotation.autowire;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {

	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("com/springcore/annotation/autowire/annotationConfig.xml");
 
		 Student student=context.getBean("student", Student.class);
		 System.out.println(student);
	}

}
